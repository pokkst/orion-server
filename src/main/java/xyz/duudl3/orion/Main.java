package xyz.duudl3.orion;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.utils.Threading;
import org.bitcoinj.wallet.DeterministicSeed;
import xyz.duudl3.orion.network.ConnectionHandler;

public class Main extends Application
{
	public static NetworkParameters params = null;
	public static final String APP_NAME = "Orion Server";
	private static String WALLET_FILE_NAME = "orion_server_wallet";
	public static WalletAppKit walletAppKit;
	public static Main instance;

	private StackPane uiStack;
	private Pane mainUI;
	public MainController controller;
	public Stage mainWindow;

	private static ConnectionHandler connHandler;
	
	public static void main(String[] args) throws IOException
	{
		params = MainNetParams.get();
		connHandler = new ConnectionHandler();
		connHandler.initialize();
		launch(args);
	}

	@Override
	public void start(Stage mainWindow) throws Exception {
		try {
			loadWindow(mainWindow);
		} catch (Throwable e) {
			throw e;
		}
	}

	private void loadWindow(Stage mainWindow) throws IOException {
		this.mainWindow = mainWindow;
		instance = this;
		FXMLLoader loader2 = new FXMLLoader();
		loader2.setLocation(getClass().getResource("main.fxml"));
		AnchorPane pane2 = loader2.<AnchorPane>load();
		controller = loader2.getController();
		Scene scene2 = new Scene(pane2);
		scene2.getStylesheets().add(getClass().getResource("styles.css").toString());
		mainWindow.setScene(scene2);
		mainWindow.setResizable(false);
		mainWindow.setMaxWidth(800);
		mainWindow.setMaxHeight(451);
		mainWindow.getIcons().add(new Image("https://duudl3.xyz/img/orion_client.png"));

		Threading.USER_THREAD = Platform::runLater;

		setupWalletKit(null);

		if (walletAppKit.isChainFileLocked()) {
			System.out.println("ERROR: Chain file locked. Exiting...");
			Platform.exit();
			return;
		}

		mainWindow.show();
	}

	public void setupWalletKit(DeterministicSeed seed) {
		// If seed is non-null it means we are restoring from backup.
		walletAppKit = new WalletAppKit(params, new File("."), WALLET_FILE_NAME)
		{
			@Override
			protected void onSetupCompleted() {
				walletAppKit.wallet().allowSpendingUnconfirmedTransactions();
				Platform.runLater(controller::onWalletSetup);
			}
		};

		walletAppKit.setDownloadListener(new DownloadProgressTracker() {
			@Override
			protected void progress(double pct, int blocksSoFar, Date date) {
				super.progress(pct, blocksSoFar, date);
				int percentage = (int) pct;
			}

			@Override
			protected void doneDownload() {
				super.doneDownload();
				//watchingWallet = Wallet.fromWatchingKey(parameters, DeterministicKey.deserializeB58(null, serialized, parameters));
			}
		});

		walletAppKit.setBlockingStartup(false).setUserAgent(APP_NAME, "1.0");

		if (seed != null)
			walletAppKit.restoreWalletFromSeed(seed);

		walletAppKit.startAsync();
	}

	public static ConnectionHandler getConnectionHandler()
	{
		return connHandler;
	}

	@Override
	public void stop() throws Exception {
		walletAppKit.stopAsync();
		walletAppKit.awaitTerminated();
		System.exit(0);
	}
}
