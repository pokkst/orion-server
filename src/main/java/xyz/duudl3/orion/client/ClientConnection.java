package xyz.duudl3.orion.client;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.MessageDigest;

import javax.xml.bind.DatatypeConverter;

public class ClientConnection {
	private Socket clientSocket;

	public ClientConnection(String server, int port) throws UnknownHostException, IOException
	{
		(new Thread() {
			@Override
			public void run() {
				try {
					System.out.println("[HOME] Initiating our client. Connecting to " + server + ":" + port);
					
					/**clientSocket = new Socket(server, port);
					sendFile("./testfolder/pic2.jpg");
					clientSocket = new Socket(server, port);
					sendFile("./testfolder/pic.jpg");
					clientSocket = new Socket(server, port);
					sendFile("./testfolder/pic3.jpg");
					clientSocket = new Socket(server, port);
					sendFile("./testfolder/test4.txt");
					clientSocket = new Socket(server, port);
					sendFile("./testfolder/test3.txt");
					clientSocket = new Socket(server, port);
					sendFile("./testfolder/test2.txt");
					clientSocket = new Socket(server, port);
					sendFile("./testfolder/test.txt");
					clientSocket = new Socket(server, port);
					sendFile("./testfolder/me.txt");**/
					
					//clientSocket = new Socket(server, port);
					//getFile("37de10b044dd3396c4f8b15496da6db072aba7d889d5f5152544bd8687eeeadb.jpg");
					
					clientSocket = new Socket(server, port);
					requestFileList();
					
					Thread.sleep(200);
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			public void sendFile(String file) throws IOException {

				DataOutputStream dos1 = new DataOutputStream(getClientConnection().getOutputStream());
				/**
				 * Notify the nodes we're sending data to of the file type we're about to send.
				 */
				dos1.writeInt(0);
				dos1.writeUTF(getFileExtension(new File(file)));

				/**
				 * Begin sending the data to the nodes.
				 */
				FileInputStream fis = new FileInputStream(file);
				File fileSending = new File(file);
				dos1.writeInt((int)fileSending.length());
				byte[] buffer = new byte[(int) fileSending.length()];

				while (fis.read(buffer) > 0) {
					dos1.write(buffer);
				}

				fis.close();
				dos1.close();	
			}

			public void getFile(String fileHash) throws IOException
			{
				DataOutputStream dos1 = new DataOutputStream(getClientConnection().getOutputStream());

				/**
				 * Notify the nodes what file we want by giving them the file hash.
				 */
				dos1.writeInt(1);
				dos1.writeUTF(fileHash);

				/**
				 * Here we receive a response from the server with the file data.
				 */
				DataInputStream dis1 = new DataInputStream(getClientConnection().getInputStream());
				//Getting file extension
				String fileExtension = dis1.readUTF();
				int fileSize = dis1.readInt();
				System.out.println("DOWNLOADED FILE EXT. " + fileExtension);

				long time = System.currentTimeMillis();
				boolean tmpFolder = new File("./client_tmp/").mkdir();
				String fileName = "./client_tmp/" + getStringHash("" + time) + fileExtension;

				/**
				 * Prepare to read the file the server is sending.
				 */
				FileOutputStream fos = new FileOutputStream(fileName);
				byte[] buffer = new byte[fileSize];

				/**
				 * Read the data.
				 */
				int read = 0;
				while ((read = dis1.read(buffer)) != -1) {
					fos.write(buffer, 0, read);
				}

				//Close the file
				fos.close();

				//Print the hash of the data that was sent from the client.
				System.out.println("Downloaded file hash: " + getFileHash(new File(fileName)).toLowerCase());

				/**
				 * Rename the file to the hash of the actual data and not our temporary fileName above. This is so other clients can retrieve
				 * the file if they have the file hash.
				 */
				File newFile = new File(fileName);
				boolean dataFolder = new File("./client_downloads/").mkdir();
				String newFileName = "./client_downloads/" + getFileHash(new File(fileName)).toLowerCase();
				File permFile = new File(newFileName + fileExtension);

				if(!permFile.exists()){
					boolean renamed = newFile.renameTo(permFile);
				}
				else{
					newFile.delete();
				}

				dis1.close();	
				dos1.close();	
			}
			
			public void requestFileList() throws IOException
			{
				DataOutputStream dos1 = new DataOutputStream(getClientConnection().getOutputStream());

				/**
				 * Notify the nodes what file we want by giving them the file hash.
				 */
				dos1.writeInt(2);

				/**
				 * Here we receive a response from the server with the file data.
				 */
				BufferedInputStream bis1 = new BufferedInputStream(getClientConnection().getInputStream());
				DataInputStream dis1 = new DataInputStream(bis1);
				int fileCount = dis1.readInt();
				
				for (int i = 0; i < fileCount; i++) 
				{
					long fileSize = dis1.readLong();
					String fileName = dis1.readUTF();
					System.out.println("File: " + fileName + " Size: " + fileSize);
				}
				
				dis1.close();	
				bis1.close();
				dos1.close();	
			}

			public String getFileHash(File input) {
				try (InputStream in = new FileInputStream(input)) {
					MessageDigest digest = MessageDigest.getInstance("SHA-256");
					byte[] block = new byte[(int)input.length()];
					int length;
					while ((length = in.read(block)) > 0) {
						digest.update(block, 0, length);
					}

					return DatatypeConverter.printHexBinary(digest.digest());
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			public String getStringHash(String value)
			{
				try{
					MessageDigest md = MessageDigest.getInstance("SHA-256");
					md.update(value.getBytes());
					return bytesToHex(md.digest());
				} catch(Exception ex){
					throw new RuntimeException(ex);
				}
			}

			private String bytesToHex(byte[] bytes)
			{    
				StringBuffer result = new StringBuffer();
				for (byte b : bytes) result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
				return result.toString();
			}

			private String getFileExtension(File file) {
				String name = file.getName();
				int lastIndexOf = name.lastIndexOf(".");
				if (lastIndexOf == -1) {
					return ""; // empty extension
				}
				return name.substring(lastIndexOf);
			}
		}).start();
	}

	public Socket getClientConnection()
	{
		return clientSocket;
	}
}
