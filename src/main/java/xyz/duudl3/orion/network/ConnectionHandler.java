package xyz.duudl3.orion.network;

import java.io.IOException;

import xyz.duudl3.orion.client.ClientConnection;
import xyz.duudl3.orion.server.ServerConnection;

public class ConnectionHandler {
	private ServerConnection serverConn;
	private ClientConnection clientConn;

	public ConnectionHandler()
	{
	}

	public void initialize() throws IOException
	{
		serverConn = new ServerConnection(NetworkConstants.PORT);
		//clientConn = new ClientConnection("73.136.173.136", NetworkConstants.PORT);
	}

	public ServerConnection getServer()
	{
		return serverConn;
	}
	
	public ClientConnection getClient()
	{
		return clientConn;
	}
}
