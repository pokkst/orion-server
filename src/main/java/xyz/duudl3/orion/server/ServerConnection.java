package xyz.duudl3.orion.server;

import xyz.duudl3.orion.Main;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.security.MessageDigest;
import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

public class ServerConnection {
	private ServerSocket serverSocket;

	public ServerConnection(int port) throws IOException
	{
		(new Thread() {
			@Override
			public void run() {
				try {
					boolean node_files = new File("./node_files/").mkdir();
					boolean node_tmp = new File("./node_tmp/").mkdir();
					System.out.println("[HOME] Binding our node service to port " + port);
					serverSocket = new ServerSocket(port);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				while(true)
				{
					try {
						Socket s = serverSocket.accept();
						DataInputStream dis1 = new DataInputStream(s.getInputStream());
						int operation = dis1.readInt();

						if(operation == 0)
						{
							saveFile(s);
						}
						else if(operation == 1)
						{
							sendFile(s);
						}
						else if(operation == 2)
						{
							sendFileList(s);
						}
						else if(operation == 3)
						{
							sendAddress(s);
						}

					} catch (IOException e) {
						e.printStackTrace();
					}	
				}
			}

			private void sendAddress(Socket clientSock) throws IOException
			{
				if(!clientSock.isClosed() && clientSock.isConnected() && !clientSock.isInputShutdown() && !clientSock.isOutputShutdown())
				{		
					BufferedOutputStream bos1 = new BufferedOutputStream(clientSock.getOutputStream());
					DataOutputStream dos1 = new DataOutputStream(bos1);	
					dos1.writeUTF(Main.walletAppKit.wallet().freshReceiveAddress().toString());

					dos1.close();
					bos1.close();
				}
				else
				{
					System.out.println("[HOME] Attempted to send Bitcoin address to client " + clientSock.getInetAddress() + " but connection has been lost.");
				}
			}
			
			private void saveFile(Socket clientSock) throws IOException {	
				System.out.println("[HOME] A client has sent us a file.");

				DataInputStream dis1 = new DataInputStream(clientSock.getInputStream());
				String fileExtension = dis1.readUTF();
				String renameToHash = dis1.readUTF();
				int fileSize = dis1.readInt();
				System.out.println("[HOME] The file's extension is: " + fileExtension);

				/**
				 * Below is a file name generation system to get unique hashes for temporary file names, to prevent mis-handling of multiple
				 * client data.
				 */

				long time = System.currentTimeMillis();
				String fileName = "./node_tmp/" + getStringHash(clientSock.getInetAddress().toString() + time) + fileExtension;

				/**
				 * Prepare to read the file the client is sending.
				 */
				FileOutputStream fos = new FileOutputStream(fileName);
				byte[] buffer = new byte[fileSize];

				/**
				 * Read the data.
				 */
				int read = 0;
				while ((read = dis1.read(buffer)) != -1) {
					fos.write(buffer, 0, read);
				}

				//Close the file
				fos.close();

				//Print the hash of the data that was sent from the client.
				System.out.println("[HOME] The file's hash is: " + renameToHash);

				/**
				 * Rename the file to the hash of the actual data and not our temporary fileName above. This is so other clients can retrieve
				 * the file if they have the file hash.
				 */
				File newFile = new File(fileName);
				String newFileName = "./node_files/" + renameToHash;
				File permFile = new File(newFileName + fileExtension);

				if(!permFile.exists()){					
					/**
					 * If the file does not exist on our server, we rename the temporary file in /node_tmp/ to the actual file in /node_files/
					 * and assume that other nodes might not have the file either, so we propagate the file.
					 */
					boolean renamed = newFile.renameTo(permFile);
				}
				else{
					newFile.delete();
				}

				dis1.close();

				System.out.println("[HOME] Done receiving file from client.");
			}

			private void sendFile(Socket clientSock) throws IOException
			{
				if(!clientSock.isClosed() && clientSock.isConnected() && !clientSock.isInputShutdown() && !clientSock.isOutputShutdown())
				{		
					DataInputStream dis1 = new DataInputStream(clientSock.getInputStream());
					String preModifiedFileName = dis1.readUTF();
					String encryptionExt = getFileExtension(new File("./node_files/" + preModifiedFileName));
					preModifiedFileName = preModifiedFileName.replace(encryptionExt, "");
					String originalExt = getFileExtension(new File("./node_files/" + preModifiedFileName));
					String hash = preModifiedFileName.replace(originalExt, "");
					System.out.println("[HOME] Client has requested file: " + preModifiedFileName);

					ArrayList<File> files = new ArrayList<File>();
					files.add(new File("./node_files/" + hash + originalExt + ".des"));
					files.add(new File("./node_files/" + hash + "_salt" + originalExt + ".enc"));
					files.add(new File("./node_files/" + hash + "_iv" + originalExt + ".enc"));
					BufferedOutputStream bos1 = new BufferedOutputStream(clientSock.getOutputStream());
					DataOutputStream dos1 = new DataOutputStream(bos1);	
					dos1.writeInt(files.size());

					for (int i = 0; i < files.size(); i++) 
					{
						String fileName = files.get(i).getName();
						System.out.println("[HOME] Sending file: " + files.get(i).getName());
						long fileSize = files.get(i).length();
						dos1.writeLong(fileSize);
						dos1.writeUTF(fileName);
						dos1.writeUTF(getFileExtension(new File("./node_files/" + fileName)));

						FileInputStream fis = new FileInputStream("./node_files/" + fileName);
						BufferedInputStream bis1 = new BufferedInputStream(fis);
						File fileSending = new File("./node_files/" + fileName);

						if(fileSending.exists())
						{
							int theByte = 0;
							while((theByte = bis1.read()) != -1) bos1.write(theByte);
						}

						bis1.close();
						fis.close();
					}

					dos1.close();
					bos1.close();
					dis1.close();
				}
				else
				{
					System.out.println("[HOME] Attempted to send file to client " + clientSock.getInetAddress() + " but connection has been lost.");
				}
			}

			private void sendFileList(Socket clientSock) throws IOException {	
				try {
					if(!clientSock.isClosed() && clientSock.isConnected() && !clientSock.isInputShutdown() && !clientSock.isOutputShutdown())
					{
						System.out.println("[HOME] Sending list of files to client.");

						File folder = new File("./node_files");
						File[] files = folder.listFiles();
						ArrayList<File> fileList = new ArrayList<File>();

						for(int x = 0; x < files.length; x++)
						{
							String fileExtension = this.getFileExtension(files[x]);

							if(fileExtension.equals(".des"))
							{
								fileList.add(files[x]);
							}
						}

						BufferedOutputStream bos1 = new BufferedOutputStream(clientSock.getOutputStream());
						DataOutputStream dos1 = new DataOutputStream(bos1);	
						dos1.writeInt(fileList.size());

						for (int i = 0; i < fileList.size(); i++) 
						{
							if (fileList.get(i).isFile())
							{
								String fileName = fileList.get(i).getName();
								System.out.println("[HOME] Sending file entry: " + fileList.get(i).getName());
								long fileSize = fileList.get(i).length();
								dos1.writeLong(fileSize);
								dos1.writeUTF(fileName);
							}
						}

						dos1.close();
						bos1.close();

						System.out.println("[HOME] Sent list of files to client.");
					}
					else
					{
						System.out.println("[HOME] Attempted to send file list to client " + clientSock.getInetAddress() + " but connection has been lost.");
					}
				} catch(SocketException e) {
					e.printStackTrace();
				}
			}

			public String getFileHash(File input) {
				try (InputStream in = new FileInputStream(input)) {
					MessageDigest digest = MessageDigest.getInstance("SHA-256");
					byte[] block = new byte[(int)input.length()];
					int length;
					while ((length = in.read(block)) > 0) {
						digest.update(block, 0, length);
					}

					return DatatypeConverter.printHexBinary(digest.digest());
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			public String getStringHash(String value)
			{
				try{
					MessageDigest md = MessageDigest.getInstance("SHA-256");
					md.update(value.getBytes());
					return bytesToHex(md.digest());
				} catch(Exception ex){
					throw new RuntimeException(ex);
				}
			}

			private String bytesToHex(byte[] bytes)
			{    
				StringBuffer result = new StringBuffer();
				for (byte b : bytes) result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
				return result.toString();
			}

			private String getFileExtension(File file) {
				String name = file.getName();
				int lastIndexOf = name.lastIndexOf(".");
				if (lastIndexOf == -1) {
					return ""; // empty extension
				}
				return name.substring(lastIndexOf);
			}
		}).start();
	}

	public ServerSocket getServerConnection()
	{
		return serverSocket;
	}
}
