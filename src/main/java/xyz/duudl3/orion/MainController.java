/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.duudl3.orion;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import org.bitcoinj.core.Address;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.wallet.SendRequest;
import org.bitcoinj.wallet.Wallet;
import org.spongycastle.util.encoders.Hex;
import xyz.duudl3.orion.tx.BroadcastHelper;
import java.io.UnsupportedEncodingException;

/**
 * Gets created auto-magically by FXMLLoader via reflection. The widget fields are set to the GUI controls they're named
 * after. This class handles all the updates and event handling for the main UI.
 */
public class MainController {
    public Label balance;
    public TextField toAddress;

    // Called by FXMLLoader.
    public void initialize() {

    }

    public void onWalletSetup() {
        String currentBalance = Main.walletAppKit.wallet().getBalance(Wallet.BalanceType.ESTIMATED).toFriendlyString();
        balance.setText(currentBalance);

        Main.walletAppKit.wallet().addChangeEventListener(Platform::runLater, wallet -> {
            String currentBalance1 = Main.walletAppKit.wallet().getBalance(Wallet.BalanceType.ESTIMATED).toFriendlyString();
            balance.setText(currentBalance1);
        });
    }

    public void withdraw(ActionEvent actionEvent) {
        String modifiedBalance = balance.getText().replace(" BTC", "");

        Coin coinAmt = Coin.parseCoin(modifiedBalance);

        if(coinAmt.getValue() > 0.0)
        {
            Address destination = Address.fromString(Main.params, toAddress.getText());
            SendRequest req = null;
            req = SendRequest.emptyWallet(destination);

            req.ensureMinRequiredFee = false;
            //converting because the UI uses sats/byte, so we need to convert that to fee per kb here
            req.feePerKb = Coin.valueOf(5L * 1000L);
            Transaction tx = null;
            try {
                tx = Main.walletAppKit.wallet().sendCoinsOffline(req);
            } catch (InsufficientMoneyException e) {
                e.printStackTrace();
            }
            byte[] txHexBytes = Hex.encode(tx.bitcoinSerialize());
            String txHex = null;
            try {
                txHex = new String(txHexBytes, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            System.out.println("Created raw transaction: " + txHex);

            System.out.println("Broadcasting raw transaction...");
            BroadcastHelper helper = new BroadcastHelper(false, false);
            helper.broadcast(txHex);
        }
    }
}
